export const blue = {

  '--primary' : '#c12f22',
  '--primary-variant': '#5c6bc0',

  '--secondary': '#b00283',
  '--secondary-variant': '#fff350',

  '--background': '#c5e1a5',
  '--surface': '#eceff1',
  '--dialog': '#f2f4f5',
  '--cancel': '#9E9E9E',
  '--alt-surface': '#d5d8d9',
  '--alt-dialog': '#455a64',


  '--on-primary': '#FFFFFF',
  '--on-secondary': '#FFFFFF',
  '--on-background': '#000000',
  '--on-surface': '#000000',
  '--on-cancel': '#000000',

  '--green': '#0D770F',
  '--red': 'red',
  '--yellow': '#b00283',
  '--blue': '#002878',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#EEEEEE',
  '--black': '#212121',
  '--moderator': '#eceff1'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Mint',
      'de': 'Mint'
    },
    'description': {
      'en': 'Contrast compliant with WCAG 2.1 AA',
      'de': 'Helligkeitskontrast nach WCAG 2.1 AA'
    }
  },
  'isDark': false,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
